#Задача#

Спарсить (программно) первые 15 новостей с rbk.ru (блок, откуда брать новости показан на скриншоте) и вставить в базу данных (составить структуру самому) или в файл. Вывести все новости, сократив текст до 200 символов в качестве описания, со ссылкой на полную новость с кнопкой подробнее. На полной новости выводить картинку если есть в новости.

#Структура#

task - директория содержит файлы задания

vendor - сторонние библиотеки

config - конфигурационные файлы

#vendor#

guzzle/guzzle - используется для загрузки страниц сайта донора

symfony/dom-crawler - парсинг загруженных страниц

#База данных#

Dump:

```mysql
CREATE TABLE IF NOT EXISTS `news` (
`id` int(11) unsigned NOT NULL,
  `source` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

ALTER TABLE `news` ADD PRIMARY KEY (`id`), ADD KEY `source` (`source`,`created`);
ALTER TABLE `news` MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
```

#Установка#

1. git clone https://iamzyxer@bitbucket.org/iamzyxer/test1.git
2. php composer.phar install
3. Создать БД
4. Создать конфиг cp config/application.dist.php config/application.local.php
5. Настроить в конфиге соединение с БД
6. Запустить импортер новостей php index.php cmd=import или test.domain.com/import