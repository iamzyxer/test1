<?php

define('DS',   DIRECTORY_SEPARATOR);
define('ROOT', __DIR__);

require_once ROOT . '/vendor/autoload.php';

class Controller
{
    protected $path;
    public $cmd;
    protected $flock;
    protected $config;
    /** @var \PDO  */
    protected $pdo;
    protected $ver = 'bnrk38';

    protected $map = [
        'index' => 'indexAction',
        'news' => 'newsAction',
        'import' => 'importAction',
    ];

    public function __construct()
    {
        $this->config = $config = $this->getConfig();
        $this->path = $config['application']['path'];

        $this->before();

        if ($this->isCli()) {
            $this->cmd = $this->getCmdForCli();
        } else {
            $this->cmd = $this->getCmdForRequest();
        }
    }

    public function before()
    {
        $config = $this->config['db'];

        $dsn = "mysql:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {
            $this->pdo = new PDO($dsn, $config['user'], $config['password'], $opt);
        } catch (PDOException $e) {
            die('Connection failed: ' . $e->getMessage());
        }
    }

    /**
     * @param null $key
     * @param null $default
     * @return array|mixed|null
     */
    public function getConfig($key = null, $default = null)
    {
        if ($this->config === null) {
            if (is_readable($path = ROOT . DS . 'config' . DS . 'application.local.php')) {
                $this->config = include_once $path;
            } elseif (is_readable($path = ROOT . DS . 'config' . DS . 'application.global.php')) {
                $this->config = include_once $path;
            } else {
                $this->config = [];
            }
        }

        if ($key === null) {
            return $this->config;
        } else {
            if (isset($this->config[$key])) {
                return $this->config[$key];
            }
        }

        return $default;
    }

    public function isCli()
    {
        return PHP_SAPI === 'cli';
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getCmdForRequest()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $request = preg_split('~\?~', $_SERVER['REQUEST_URI']);
            $request = $request[0];
        } elseif (!empty($_SERVER['PATH_INFO'])) {
            $request = $_SERVER['PATH_INFO'];
        } elseif (isset($_SERVER['PHP_SELF'])) {
            $request = str_replace('index.php', '', $_SERVER['PHP_SELF']);
        } else {
            throw new \Exception('Unable to detect the URI using REQUEST_URI, PATH_INFO, PHP_SELF');
        }

        if (!empty($this->path)) {
            $request = preg_replace('~^' . preg_quote($this->path) . '~i', '', $request);
        }

        return trim($request, '/');
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getCmdForCli()
    {
        if (empty($_SERVER['argv'])) {
            throw new \Exception('Controller not found');
        }

        $arguments = array();

        foreach ($_SERVER['argv'] as $arg) {
            if (strpos($arg, '=') !== false) {
                $_arg = explode('=', $arg);

                $arguments[$_arg[0]] = $_arg[1];
            }
        }

        return empty($arguments['cmd']) ? null : $arguments['cmd'];
    }

    /**
     * @return string
     */
    public function getVer()
    {
        return $this->ver;
    }

    /**
     * @param null $cmd
     * @param string $default
     */
    public function forward($cmd = null, $default = 'index')
    {
        $cmd = empty($cmd) ? $this->cmd : $cmd;
        if (empty($cmd)) {
            $this->cmd = $cmd = $default;
        }

        if (empty($this->map[$cmd])) {
            if ($this->isCli()) {
                print 'Command not found';
                exit();
            }
            $this->notFoundAction();
        }

        $method = $this->map[$cmd];
        if ($this->isCli()) {
            $this->$method();
            exit();
        }

        header(sprintf('Content-type: %s; charset=%s', 'text/html', 'utf-8'));
        print $this->$method();
        exit();
    }

    protected function getHeaderHtml()
    {
        include 'phtml/partial/header.phtml';
    }

    protected function getFooterHtml()
    {
        include 'phtml/partial/footer.phtml';
    }

    protected function notFoundAction()
    {
        header((empty($_SERVER['SERVER_PROTOCOL']) ? 'HTTP/1.1' : $_SERVER['SERVER_PROTOCOL']) . ' 404 Not Found', true, 404);
        include 'phtml/404.phtml';
        exit();
    }

    /**
     * @return string
     */
    protected function indexAction()
    {
        $this->getHeaderHtml();
        include 'phtml/index.phtml';
        $this->getFooterHtml();
    }

    /**
     * @return string
     */
    protected function newsAction()
    {
        $id = empty($_REQUEST['id']) ? null : (int)$_REQUEST['id'];

        $this->getHeaderHtml();

        $stmt = $this->pdo->prepare('SELECT * FROM `news` WHERE `id` = :id');
        $stmt->execute(['id' => $id]);
        $news = $stmt->fetch();

        if (empty($news)) {
            $this->notFoundAction();
        }

        include 'phtml/news.phtml';
        $this->getFooterHtml();
    }

    protected function importAction()
    {
        if (!$this->isCli()) echo '<pre>';


        $this->log('Start importing news from https://www.rbc.ru/');

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://www.rbc.ru/');

        if ($res->getStatusCode() != 200) {
            $this->log('ERR, rbc.ru not found');
        }

        $html = $res->getBody()->getContents();

        $crawler = new \Symfony\Component\DomCrawler\Crawler($html);

        $items = $crawler->filter('.news-feed__item');
        $i = 0;
        foreach ($items as $domElement) {
            $i++;
            $_crawler = new \Symfony\Component\DomCrawler\Crawler($domElement);

            $this->log("ADD #{$i}: \t" . trim($_crawler->filter('.news-feed__item__title')->text()));
            $this->addNews($client, $_crawler->attr('href'));
            if ($i >= 15) break;
        }

        $this->log('Import is over');
    }

    /**
     * @param \GuzzleHttp\Client $client
     * @param $source
     * @return bool
     */
    protected function addNews(GuzzleHttp\Client $client, $source)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM `news` WHERE `source` = :source');
        $stmt->execute(['source' => $source]);
        $res = $stmt->fetch();

        if (empty($res)) {
            $res = $client->request('GET', $source);

            if ($res->getStatusCode() != 200) {
                $this->log('ERR, ' . $source . ' not found');
            }

            $html = $res->getBody()->getContents();
            $crawler = new \Symfony\Component\DomCrawler\Crawler($html);

            try {
                $title = $crawler->filter('.article__header__title');
                if (!empty($title)) {
                    $title = trim(strip_tags($title->html()));
                }
            } catch (\Exception $e) {
                $this->log('--SKIP');
                return false;
            }

            try {
                $body = $crawler->filter('.article__text');
                if (!empty($body)) {
                    $body = $body->html();
                }
            } catch (\Exception $e) {
                $this->log('--SKIP');
                return false;
            }

            $image = null;
            try {
                $_image = $crawler->filter('.article__main-image img');
                if (!empty($_image)) {
                    $image = $_image->attr('src');
                }
            } catch (\Exception $e) {
            }

            $sql = "INSERT INTO `news`
                    SET
                    `source` = :source,
                    `title` = :title,
                    `body` = :body,
                    `image` = :image,
                    `created` = NOW(),
                    `updated` = NOW()";
            $stm = $this->pdo->prepare($sql);
            $stm->execute([
                'source' => $source,
                'title' => $title,
                'body' => $body,
                'image' => $image
            ]);

            $this->log('--DONE');
        } else {
            $this->log('--HAS_ALREADY');
        }
    }

    /**
     * @return mixed
     */
    public function getNews()
    {
        $stmt = $this->pdo->prepare('SELECT * FROM `news` ORDER BY `created` DESC');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    protected function log($message)
    {
        echo "\t" . $message . PHP_EOL;
    }

    /**
     * @param $path
     * @return string
     */
    public function uri($path)
    {
        return $this->path . $path;
    }

    public function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false)
    {
        if ($length == 0) return '';

        if (mb_strlen($string, 'UTF-8') > $length) {
            $length -= min($length, mb_strlen($etc, 'UTF-8'));
            if (!$break_words && !$middle) {
                $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, $length + 1, 'UTF-8'));
            }
            if (!$middle) {
                return mb_substr($string, 0, $length, 'UTF-8') . $etc;
            }

            return mb_substr($string, 0, $length / 2, 'UTF-8') . $etc . mb_substr($string, - $length / 2, $length, 'UTF-8');
        }
        return $string;
    }
}


$controller = new Controller();
$config = $controller->getConfig('application');
$controller->forward(null, $config['default']);